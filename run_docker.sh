# RUN DOCKER
docker stop ur5

docker rm ur5

docker run --ipc=host --gpus all --runtime=runc --interactive -it \
--shm-size=10gb \
--env="DISPLAY" \
--volume="${PWD}:/home/ur5_user" \
--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
--volume="/dev:/dev" \
--workdir="/home/ur5_user" \
--privileged \
--name=ur5 ur5:latest

# docker exec -it ur5 bash
