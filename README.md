# ur5_ws

Note: This was tested only with ROS Noetic. 

## Installation

1. Install ROS: http://wiki.ros.org/ROS/Installation
2. Install the following libraries:
```bash
sudo apt-get install ros-$ROS_DISTRO-realsense2-camera
sudo apt-get install ros-$ROS_DISTRO-openni-launch
sudo apt-get install ros-$ROS_DISTRO-openni2-launch
sudo apt-get install ros-$ROS_DISTRO-ros-numpy
sudo apt-get install ros-$ROS_DISTRO-rosbash
sudo apt-get install ros-$ROS_DISTRO-ros-control
sudo apt-get install ros-$ROS_DISTRO-soem
sudo apt-get install ros-$ROS_DISTRO-moveit
sudo apt-get install ros-$ROS_DISTRO-trac-ik
```
3. Install the following python libs:
```bash
pip3 install pymodbus --upgrade
pip3 install ur-rtde
pip3 install pyyaml
```
4. Clone the repo:
```bash
git clone https://gitlab.com/vsimundic/ur5_ws.git
```
4. Build the workspace:
```bash
cd ur5_ws
rosdep update
rosdep install --from-paths src --ignore-src -r --rosdistro $ROS_DISTRO -y
catkin_make
```

